<?php

namespace spec\SelworkBundle\Listener;

use SelworkBundle\Factory\RequestFactory;
use SelworkBundle\Listener\RequestListener;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use SelworkBundle\Model\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @mixin RequestListener
 */
class RequestListenerSpec extends ObjectBehavior
{
    public function let(AuthorizationCheckerInterface $authorizationChecker, RequestFactory $requestFactory)
    {
        $this->beConstructedWith($authorizationChecker, $requestFactory);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(RequestListener::class);
    }


    public function it_should_store_request_when_authenticated_and_master_request(
        GetResponseEvent $event,
        SymfonyRequest $symfonyRequest,
        AuthorizationCheckerInterface $authorizationChecker,
        Request $request,
        RequestFactory $requestFactory
    ) {
        $authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')->willReturn(true);

        $event->isMasterRequest()->willReturn(true);
        $event->getRequest()->willReturn($symfonyRequest);

        $requestFactory->createFromSymfonyRequest($symfonyRequest)->willReturn($request);

        $request->save()->shouldBeCalled();

        $this->onKernelRequest($event);
    }
}
