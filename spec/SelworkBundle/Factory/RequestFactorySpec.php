<?php

namespace spec\SelworkBundle\Factory;

use SelworkBundle\Factory\RequestFactory;
use spec\SelworkBundle\Stubs\RequestStub;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use SelworkBundle\Model\Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Session\Session;

require(__DIR__.'/../Stubs/RequestStub.php');
/**
 * @mixin RequestFactory
 */
class RequestFactorySpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(RequestFactory::class);
    }

    public function it_should_create_request_from_symfony_request()
    {
        $requstStub = new RequestStub();

        $this->createFromSymfonyRequest($requstStub)->shouldBeAnInstanceOf(Request::class);
    }
}
