<?php

namespace SelworkBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SelworkSendStatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('selwork:send-stats')
            ->setDescription('Send requests statistics to your email')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $date->modify('-1 hour');
        $this->getContainer()->get('selworks.statistics_sender')->send($date);
    }
}
