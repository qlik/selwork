<?php


namespace SelworkBundle\Factory;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use SelworkBundle\Model\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RequestFactory
{
    /**
     * @param SymfonyRequest $symfonyRequest
     * @return Request
     */
    public function createFromSymfonyRequest(SymfonyRequest $symfonyRequest)
    {
        $request = new Request();

        if ($symfonyRequest->getSession() instanceof SessionInterface) {
            $request->setSessionId($symfonyRequest->getSession()->getId());
        }

        $request->setIpAddress($symfonyRequest->getClientIp());
        $request->setRequestUrl(json_encode($symfonyRequest->request->all()));
        $request->setPostParameters(json_encode($symfonyRequest->request->all()));
        $request->setGetParameters(json_encode($symfonyRequest->query->all()));
        $request->setMethod($symfonyRequest->getMethod());
        $request->setOriginalFileName($symfonyRequest->files->get('tmp_name'));
        $request->setTemporaryFileName($symfonyRequest->files->get('tpm_name'));
        $request->setReferrer($symfonyRequest->headers->get('referer'));

        return $request;
    }
}
