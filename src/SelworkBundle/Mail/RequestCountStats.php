<?php


namespace SelworkBundle\Mail;

use SelworkBundle\Model\RequestQuery;

class RequestCountStats
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * RequestCountStats constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function send(\DateTime $dateTime)
    {
        $now = time();
        $count = RequestQuery::create()->getCountSince($dateTime); //
        $diff = $now - $dateTime->getTimestamp();
        $body = sprintf(
            "Request count since %s (last %d seconds) is %d",
            $dateTime->format('Y-m-d H:i:s'),
            $diff,
            $count
        );

        $message = $this->mailer->createMessage()
            ->setSubject('Request statistics')
            ->setFrom('send@example.com')
            ->setTo('recipient@example.com')
            ->setBody($body);

        $this->mailer->send($message);
    }
}
