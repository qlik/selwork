<?php

namespace SelworkBundle\Model;

use SelworkBundle\Model\om\BaseRequest;

class Request extends BaseRequest
{
    public function preInsert(\PropelPDO $con = null)
    {
        $this->setCreatedAt(time());
        return true;
    }
}
