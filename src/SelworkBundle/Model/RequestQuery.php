<?php

namespace SelworkBundle\Model;

use SelworkBundle\Model\om\BaseRequestQuery;
use Symfony\Component\Validator\Constraints\DateTime;

class RequestQuery extends BaseRequestQuery
{
    /**
     * @param \DateTime $date
     * @return RequestQuery
     */
    public function since(\DateTime $date)
    {
        return $this->filterByCreatedAt(array('min' => time() - $date->getTimestamp()));
    }

    /**
     * @param \DateTime $date
     * @return int
     */
    public function getCountSince(\DateTime $date)
    {
        return $this->since($date)->count();
    }
}
