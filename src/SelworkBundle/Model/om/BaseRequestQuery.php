<?php

namespace SelworkBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use SelworkBundle\Model\Request;
use SelworkBundle\Model\RequestPeer;
use SelworkBundle\Model\RequestQuery;

/**
 * @method RequestQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RequestQuery orderBySessionId($order = Criteria::ASC) Order by the session_id column
 * @method RequestQuery orderByIpAddress($order = Criteria::ASC) Order by the ip_address column
 * @method RequestQuery orderByRequestUrl($order = Criteria::ASC) Order by the request_url column
 * @method RequestQuery orderByPostParameters($order = Criteria::ASC) Order by the post_parameters column
 * @method RequestQuery orderByGetParameters($order = Criteria::ASC) Order by the get_parameters column
 * @method RequestQuery orderByMethod($order = Criteria::ASC) Order by the method column
 * @method RequestQuery orderByOriginalFileName($order = Criteria::ASC) Order by the original_file_name column
 * @method RequestQuery orderByTemporaryFileName($order = Criteria::ASC) Order by the temporary_file_name column
 * @method RequestQuery orderByReferrer($order = Criteria::ASC) Order by the referrer column
 * @method RequestQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 *
 * @method RequestQuery groupById() Group by the id column
 * @method RequestQuery groupBySessionId() Group by the session_id column
 * @method RequestQuery groupByIpAddress() Group by the ip_address column
 * @method RequestQuery groupByRequestUrl() Group by the request_url column
 * @method RequestQuery groupByPostParameters() Group by the post_parameters column
 * @method RequestQuery groupByGetParameters() Group by the get_parameters column
 * @method RequestQuery groupByMethod() Group by the method column
 * @method RequestQuery groupByOriginalFileName() Group by the original_file_name column
 * @method RequestQuery groupByTemporaryFileName() Group by the temporary_file_name column
 * @method RequestQuery groupByReferrer() Group by the referrer column
 * @method RequestQuery groupByCreatedAt() Group by the created_at column
 *
 * @method RequestQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RequestQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RequestQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Request findOne(PropelPDO $con = null) Return the first Request matching the query
 * @method Request findOneOrCreate(PropelPDO $con = null) Return the first Request matching the query, or a new Request object populated from the query conditions when no match is found
 *
 * @method Request findOneBySessionId(string $session_id) Return the first Request filtered by the session_id column
 * @method Request findOneByIpAddress(string $ip_address) Return the first Request filtered by the ip_address column
 * @method Request findOneByRequestUrl(string $request_url) Return the first Request filtered by the request_url column
 * @method Request findOneByPostParameters(string $post_parameters) Return the first Request filtered by the post_parameters column
 * @method Request findOneByGetParameters(string $get_parameters) Return the first Request filtered by the get_parameters column
 * @method Request findOneByMethod(string $method) Return the first Request filtered by the method column
 * @method Request findOneByOriginalFileName(string $original_file_name) Return the first Request filtered by the original_file_name column
 * @method Request findOneByTemporaryFileName(string $temporary_file_name) Return the first Request filtered by the temporary_file_name column
 * @method Request findOneByReferrer(string $referrer) Return the first Request filtered by the referrer column
 * @method Request findOneByCreatedAt(string $created_at) Return the first Request filtered by the created_at column
 *
 * @method array findById(int $id) Return Request objects filtered by the id column
 * @method array findBySessionId(string $session_id) Return Request objects filtered by the session_id column
 * @method array findByIpAddress(string $ip_address) Return Request objects filtered by the ip_address column
 * @method array findByRequestUrl(string $request_url) Return Request objects filtered by the request_url column
 * @method array findByPostParameters(string $post_parameters) Return Request objects filtered by the post_parameters column
 * @method array findByGetParameters(string $get_parameters) Return Request objects filtered by the get_parameters column
 * @method array findByMethod(string $method) Return Request objects filtered by the method column
 * @method array findByOriginalFileName(string $original_file_name) Return Request objects filtered by the original_file_name column
 * @method array findByTemporaryFileName(string $temporary_file_name) Return Request objects filtered by the temporary_file_name column
 * @method array findByReferrer(string $referrer) Return Request objects filtered by the referrer column
 * @method array findByCreatedAt(string $created_at) Return Request objects filtered by the created_at column
 */
abstract class BaseRequestQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRequestQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = 'SelworkBundle\\Model\\Request', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RequestQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RequestQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RequestQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RequestQuery) {
            return $criteria;
        }
        $query = new RequestQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Request|Request[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RequestPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RequestPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Request A model object, or null if the key is not found
     * @throws PropelException
     */
    public function findOneById($key, $con = null)
    {
        return $this->findPk($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Request A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `session_id`, `ip_address`, `request_url`, `post_parameters`, `get_parameters`, `method`, `original_file_name`, `temporary_file_name`, `referrer`, `created_at` FROM `request` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Request();
            $obj->hydrate($row);
            RequestPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Request|Request[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Request[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        return $this->addUsingAlias(RequestPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        return $this->addUsingAlias(RequestPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RequestPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RequestPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RequestPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the session_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySessionId('fooValue');   // WHERE session_id = 'fooValue'
     * $query->filterBySessionId('%fooValue%'); // WHERE session_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sessionId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterBySessionId($sessionId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sessionId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sessionId)) {
                $sessionId = str_replace('*', '%', $sessionId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::SESSION_ID, $sessionId, $comparison);
    }

    /**
     * Filter the query on the ip_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ip_address = 'fooValue'
     * $query->filterByIpAddress('%fooValue%'); // WHERE ip_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipAddress)) {
                $ipAddress = str_replace('*', '%', $ipAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::IP_ADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the request_url column
     *
     * Example usage:
     * <code>
     * $query->filterByRequestUrl('fooValue');   // WHERE request_url = 'fooValue'
     * $query->filterByRequestUrl('%fooValue%'); // WHERE request_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $requestUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByRequestUrl($requestUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($requestUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $requestUrl)) {
                $requestUrl = str_replace('*', '%', $requestUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::REQUEST_URL, $requestUrl, $comparison);
    }

    /**
     * Filter the query on the post_parameters column
     *
     * Example usage:
     * <code>
     * $query->filterByPostParameters('fooValue');   // WHERE post_parameters = 'fooValue'
     * $query->filterByPostParameters('%fooValue%'); // WHERE post_parameters LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postParameters The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByPostParameters($postParameters = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postParameters)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $postParameters)) {
                $postParameters = str_replace('*', '%', $postParameters);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::POST_PARAMETERS, $postParameters, $comparison);
    }

    /**
     * Filter the query on the get_parameters column
     *
     * Example usage:
     * <code>
     * $query->filterByGetParameters('fooValue');   // WHERE get_parameters = 'fooValue'
     * $query->filterByGetParameters('%fooValue%'); // WHERE get_parameters LIKE '%fooValue%'
     * </code>
     *
     * @param     string $getParameters The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByGetParameters($getParameters = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($getParameters)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $getParameters)) {
                $getParameters = str_replace('*', '%', $getParameters);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::GET_PARAMETERS, $getParameters, $comparison);
    }

    /**
     * Filter the query on the method column
     *
     * Example usage:
     * <code>
     * $query->filterByMethod('fooValue');   // WHERE method = 'fooValue'
     * $query->filterByMethod('%fooValue%'); // WHERE method LIKE '%fooValue%'
     * </code>
     *
     * @param     string $method The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByMethod($method = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($method)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $method)) {
                $method = str_replace('*', '%', $method);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::METHOD, $method, $comparison);
    }

    /**
     * Filter the query on the original_file_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalFileName('fooValue');   // WHERE original_file_name = 'fooValue'
     * $query->filterByOriginalFileName('%fooValue%'); // WHERE original_file_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalFileName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByOriginalFileName($originalFileName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalFileName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $originalFileName)) {
                $originalFileName = str_replace('*', '%', $originalFileName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::ORIGINAL_FILE_NAME, $originalFileName, $comparison);
    }

    /**
     * Filter the query on the temporary_file_name column
     *
     * Example usage:
     * <code>
     * $query->filterByTemporaryFileName('fooValue');   // WHERE temporary_file_name = 'fooValue'
     * $query->filterByTemporaryFileName('%fooValue%'); // WHERE temporary_file_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $temporaryFileName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByTemporaryFileName($temporaryFileName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($temporaryFileName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $temporaryFileName)) {
                $temporaryFileName = str_replace('*', '%', $temporaryFileName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::TEMPORARY_FILE_NAME, $temporaryFileName, $comparison);
    }

    /**
     * Filter the query on the referrer column
     *
     * Example usage:
     * <code>
     * $query->filterByReferrer('fooValue');   // WHERE referrer = 'fooValue'
     * $query->filterByReferrer('%fooValue%'); // WHERE referrer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referrer The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByReferrer($referrer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referrer)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $referrer)) {
                $referrer = str_replace('*', '%', $referrer);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RequestPeer::REFERRER, $referrer, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RequestPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RequestPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RequestPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Request $request Object to remove from the list of results
     *
     * @return RequestQuery The current query, for fluid interface
     */
    public function prune($request = null)
    {
        if ($request) {
            $this->addUsingAlias(RequestPeer::ID, $request->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }
}
