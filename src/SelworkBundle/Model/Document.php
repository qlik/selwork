<?php


namespace SelworkBundle\Model;

class Document
{
    private $file;
    /**
 * @return mixed
 */
    public function getFile()
    {
        return $this->file;
    }/**
 * @param mixed $file
 * @return Document
 */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }
}
