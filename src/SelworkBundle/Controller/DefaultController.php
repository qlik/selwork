<?php

namespace SelworkBundle\Controller;

use SelworkBundle\Form\UploadType;
use SelworkBundle\Model\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('SelworkBundle:Default:index.html.twig');
    }

    /**
     * @Route("/admin")
     */
    public function securedAction()
    {
        return $this->render('SelworkBundle:Default:secured.html.twig');
    }
    /**
     * @Route("/admin/upload")
     */
    public function uploadAction(Request $request)
    {
        $document = new Document();
        $form = $this->createForm(UploadType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */

            $file = $document->getFile();

            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            $file->move(
                $this->getParameter('kernel.root_dir').'/../web/uploads',
                $fileName
            );
        }
        return $this->render('SelworkBundle:Default:upload.html.twig', ['form' => $form->createView()]);
    }
}
